﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Postpreprocess.aspx.cs" Inherits="WebApplication1.Postpreprocess" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Road Anomaly | Post Preprocess</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />

    <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet" />

    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="page-wrapper" class="gray-bg" style="margin: 0 0 0 0px;">

                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>Preprocessed Data</h2>

                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>

                <!-- <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>Preprocessed Data</h2>

                    </div>
                    <div class="col-lg-2">
                    </div>
                </div> -->

                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox ">
                                <div class="ibox-title">
                                    <asp:Label ID="lblCount" runat="server" CssClass="label"></asp:Label>
                                    <asp:Button ID="btnNext" runat="server" Text="Procceed" CssClass="btn btn-primary" OnClick="btnNext_Click" />
                                </div>
                                <div class="ibox-content">


                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-example  ">
                                            <asp:Repeater ID="rptData" runat="server">
                                                <HeaderTemplate>

                                                    <thead>
                                                        <tr class="gradeX">
                                                            <th>No.</th>
                                                            <th>No. of vehicles</th>
                                                            <th>Date</th>
                                                            <th>Road</th>
                                                            <th>Road Surface</th>
                                                            <th>Lighting condition</th>
                                                            <th>Weather Condition</th>
                                                            <th>Area</th>
                                                            <th>Gender</th>
                                                            <th>Age</th>
                                                            <th>Vehicle type</th>
                                                        </tr>

                                                        
                                                    </thead>
                                                    <tbody>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr class="gradeX">
 <td><%# Eval("Reference_No")%></td>
                                                       
                                                        <td>
                                                            <%#Eval("Numberof_vehicles") %>
                                                        </td>

                                                        <td>
                                                            <%#Eval("Accident_Date") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("Road") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("Road_surface") %>
                                                        </td>

                                                        <td>
                                                            <%#Eval("lighting_condition") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("weather_condition") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("Area") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("Gender") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("Age") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("Type_vehicle") %>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </tbody>
                                                   </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>



        <!-- Mainly scripts -->
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <script src="js/plugins/dataTables/datatables.min.js"></script>
        <script src="js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="js/inspinia.js"></script>
        <script src="js/plugins/pace/pace.min.js"></script>

        <!-- Page-Level Scripts -->
        <script>
            $(document).ready(function () {
                $('.dataTables-example').DataTable({
                    pageLength: 25,
                    responsive: true,
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                        { extend: 'copy' },
                        { extend: 'csv' },
                        { extend: 'excel', title: 'ExampleFile' },
                        { extend: 'pdf', title: 'ExampleFile' },

                        {
                            extend: 'print',
                            customize: function (win) {
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            }
                        }
                    ]

                });

            });

        </script>



    </form>
</body>
</html>
