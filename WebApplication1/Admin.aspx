﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="WebApplication1.Admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="icon" type="image/png" sizes="40x40" href="images/logo/3.png">
    <title></title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/default.css" rel="stylesheet" />
    <link href="css/plugins/footable/footable.core.css" rel="stylesheet" />
    <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet" />
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet" />
    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>




    <script src="js/plugins/pace/pace.min.js"></script>
    <script src="js/plugins/iCheck/icheck.min.js"></script>
    <!-- Sparkline -->



    <script src="js/plugins/dataTables/datatables.min.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).on("click", "#btnSubmit", function () {
            var pass = document.getElementById("txtpass");
            var lbl=document.getElementById("lblerror");
            if (pass.value == "admin")
                $('#mymodal').modal('hide');
            else
                lbl.innerHTML = "Incorrect password";
});
    </script>
    <script>
        $(document).ready(function () {

            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy' },
                    { extend: 'csv' },
                    { extend: 'excel', title: 'ExampleFile' },
                    { extend: 'pdf', title: 'ExampleFile' },

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                $('.dataTables-example').DataTable({
                    pageLength: 25,
                    responsive: true,
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                        { extend: 'copy' },
                        { extend: 'csv' },
                        { extend: 'excel', title: 'ExampleFile' },
                        { extend: 'pdf', title: 'ExampleFile' },

                        {
                            extend: 'print',
                            customize: function (win) {
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            }
                        }
                    ]

                });

            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            var prm2 = Sys.WebForms.PageRequestManager.getInstance();
            prm2.add_endRequest(function () {

                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });

            });
        });
    </script>
</head>

<body>
    <form id="form1" runat="server">
        <div class="gray-bg">
            <div class="wrapper wrapper-content animated fadeInRight " style="min-height: -webkit-fill-available;">

                <div class="modal fade in" id="mymodal" runat="server" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <%--<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>--%>
                                <h4 class="modal-title">Admin login</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="form-control">
                                        <asp:Label ID="lblPass" runat="server" Text="Enter admin password"></asp:Label>
                                        <input type="password" id="txtpass" class="form-control" />
                                        <label id="lblerror"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="btnSubmit" class="btn btn-primary"  >Submit</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h2>Admin Panel</h2>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <asp:Repeater ID="rptData" runat="server">
                                        <HeaderTemplate>
                                            <table class="table table-striped table-hover dataTables-example">
                                                <thead style="background-color: #596361; color: white;">
                                                    <tr class="gradeX">
                                                        <th></th>
                                                        <th>No.</th>
                                                        <th>No. of vehicles</th>
                                                        <th>Date</th>
                                                        <th>Road</th>
                                                        <th>Road Surface</th>
                                                        <th>Lighting condition</th>
                                                        <th>Weather Condition</th>
                                                        <th>Area</th>
                                                        <th>Gender</th>
                                                        <th>Age</th>
                                                        <th>Vehicle type</th>

                                                       


                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>

                                        <ItemTemplate>


                                            <tr class="gradeX">
                                                <td>
                                                    <asp:CheckBox ID="chkCheck" runat="server" CssClass="i-checks" /></td>
                                                <td><%# Eval("no")%></td>
                                                <asp:HiddenField runat="server" ID="hidden" Value='<%# Eval("no") %>' />
                                                <td>
                                                    <%#Eval("no_of_vehicles") %>
                                                </td>

                                                <td>
                                                    <%#Eval("Date") %>
                                                </td>
                                                <td>
                                                    <%#Eval("Road") %>
                                                </td>
                                                <td>
                                                    <%#Eval("Road_surface") %>
                                                </td>

                                                <td>
                                                    <%#Eval("lighting_condition") %>
                                                </td>
                                                <td>
                                                    <%#Eval("weather_condition") %>
                                                </td>
                                                <td>
                                                    <%#Eval("Area") %>
                                                </td>
                                                <td>
                                                    <%#Eval("Gender") %>
                                                </td>
                                                <td>
                                                    <%#Eval("Age") %>
                                                </td>
                                                <td>
                                                    <%#Eval("Vehicle_type") %>
                                                </td>

                                            </tr>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>  
                                            </table> 
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <asp:Button ID="btnAccept" runat="server" Text="Accept" CssClass="btn btn-info btn-rounded" OnClick="btnAccept_Click" />
                                        <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-danger btn-rounded mrg-lft" OnClick="btnReject_Click" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <br />
                            <br />

                        </div>

                    </div>


                </div>
            </div>
        </div>
    </form>

    <script type="text/javascript">
    $(window).on('load',function(){
        $('#mymodal').modal({backdrop: 'static', keyboard: false});
        });

       
       
</script>
</body>
</html>
