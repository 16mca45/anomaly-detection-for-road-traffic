﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Preprocess : System.Web.UI.Page
    {
        DBC obj = new DBC();
        protected void Page_Load(object sender, EventArgs e)
        {
            obj.ExcecuteQuery("Select * from Traffic_data");
            rptData.DataSource = obj.DT;
            rptData.DataBind();
            lblCount.Text = "Count: " + obj.DT.Rows.Count.ToString();
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
           // obj.ExcecuteNonQuery("Delete from Traffic_data where Date not like '[0-9][0-9]/%'");

            obj.ExcecuteNonQuery("Update Traffic_data set Road= REPLACE(Road,'\"',''),Road_surface= REPLACE(Road_surface,'\"',''),type_vehicle= REPLACE(type_vehicle,'\"','')");
            //obj.ExcecuteNonQuery("Update Traffic_data set item= REPLACE(item,';','')");
            Response.Redirect("PostPreprocess.aspx");

        }
    }
}