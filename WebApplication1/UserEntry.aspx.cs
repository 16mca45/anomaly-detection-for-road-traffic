﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    
    public partial class UserEntry : System.Web.UI.Page
    {
      
        
        DBC obj = new DBC();
        DBC obj1 = new DBC();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlRdSrface.Items.Add("Select");
            obj.ExcecuteQuery("select distinct Road_surface from Traffic_data");
            for(int i = 0; i < obj.DT.Rows.Count; i++)
            {
                ddlRdSrface.Items.Add(obj.DT.Rows[i][0].ToString());
            }
            ddlLightCondition.Items.Add("Select");
            ddlLightCondition.Items.Add("DAWN");
            ddlLightCondition.Items.Add("DRY");
            ddlLightCondition.Items.Add("ICE");
            ddlLightCondition.Items.Add("SAND");
            ddlLightCondition.Items.Add("WET");

            ddlWeatherCondtn.Items.Add("Select");
            ddlWeatherCondtn.Items.Add("DIRT");
            ddlWeatherCondtn.Items.Add("CLEAR");

            ddlWeatherCondtn.Items.Add("CLOUDY");

            ddlWeatherCondtn.Items.Add("FOG");

           ddlWeatherCondtn.Items.Add("RAIN");
            ddlWeatherCondtn.Items.Add("SLEET");
     






            ddlGender.Items.Add("Select");
            ddlGender.Items.Add("Male");
            ddlGender.Items.Add("Female");

            ddlVehType.Items.Add("Select");
            ddlVehType.Items.Add("PASSENGER CAR");
            ddlVehType.Items.Add("SPORT UTILITY");
            ddlVehType.Items.Add("VAN");
            ddlVehType.Items.Add("PICKUP");
            ddlVehType.Items.Add("POLICE");
            ddlVehType.Items.Add("SINGLE UNIT TRUCK");
            ddlVehType.Items.Add("LIGHT TRUCK");
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int no;
          
                obj1.ExcecuteQuery("select max(no) from Table_1");
                if(obj1.DT.Rows.Count>0 && obj1.DT.Rows[0][0].ToString()!="")
                {
                    no = Convert.ToInt32(obj1.DT.Rows[0][0].ToString()) + 1;

                }
                else
                {
                    no = 1;
                }
                obj.ExcecuteNonQuery("insert into Table_1 values('"+no+"','" +txtNofVehicles.Text + "','"+txtDate.Text+"','" + txtRoad.Text + "','" + ddlRdSrface.Text + "','" + ddlLightCondition.Text + "','" + ddlWeatherCondtn.Text + "','" + txtArea.Text + "','" + ddlGender.Text + "','" + txtAge.Text + "','" + ddlVehType.Text + "') ");

                txtNofVehicles.Text = "";
                txtRoad.Text = "";
                ddlRdSrface.SelectedIndex = 0;
                ddlLightCondition.SelectedIndex = 0;
                ddlWeatherCondtn.SelectedIndex = 0;
                txtArea.Text = "";
                ddlGender.SelectedIndex = 0;
                txtAge.Text = "";
                ddlVehType.SelectedIndex = 0;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data entered successfully!')", true);



        }

       
    }
}