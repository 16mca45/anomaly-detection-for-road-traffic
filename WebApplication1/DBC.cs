﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;

namespace WebApplication1
{
    public class DBC
    {
        public SqlCommand sqlcommand;
        public DataSet ds = new DataSet();
        public static string connstring = System.Configuration.ConfigurationManager.ConnectionStrings["DbConn"].ConnectionString;
        public SqlConnection conn = new SqlConnection(connstring);
        public SqlDataAdapter DA;
        public DataTable DT = new DataTable();

        
        public string OpenConnection()
        {
            try
            {
                conn.Open();

                return "Success";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

          
        // public string OpenConnection()
        // {
        //     try
        //     {
        //         conn.Open();

        //         return "Success";

        //     }
        //     catch (Exception ex)
        //     {
        //         return ex.Message;
        //     }
        // }

        

        public DataTable GetDataTable(string strquery)
        {
            SqlCommand command = new SqlCommand(strquery, conn);
            SqlDataAdapter ODA = new SqlDataAdapter(command);
            try
            {
                DataTable dtTemp = new DataTable();
                ODA.Fill(dtTemp);

                return dtTemp;
            }
            catch (Exception ex) { return null; }

            finally
            {
                conn.Close();
            }

        }

        public void ExcecuteQuery(string strquery)
        {
            SqlCommand command = new SqlCommand(strquery, conn);
            SqlDataAdapter ODA = new SqlDataAdapter(command);
            try
            {
                ODA.Fill(ds);
                ODA.Fill(DT);

            }
            catch (Exception ex) { }

            conn.Close();

        }
        public void ExcecuteNonQuery(string strquery)
        {
            try
            {
                conn.Open();

            }
            catch (Exception ex)
            {

            }

            SqlCommand obj = new SqlCommand(strquery, conn);
            obj.ExecuteNonQuery();
            conn.Close();
        }


        public void ExcecuteQuery_SP(SqlCommand command)
        {
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = conn;
            SqlDataAdapter ODA = new SqlDataAdapter(command);
            try
            {
                ODA.Fill(ds);
                ODA.Fill(DT);

            }
            catch (Exception ex) { }

            conn.Close();

        }

        public void ExcecuteNonQuery_SP (SqlCommand command)
        {
            try
            {
                conn.Open();

            }
            catch (Exception ex)
            {

            }

            command.CommandType = CommandType.StoredProcedure;
            command.Connection = conn;
            command.ExecuteNonQuery();
            conn.Close();
        }
    }
}
