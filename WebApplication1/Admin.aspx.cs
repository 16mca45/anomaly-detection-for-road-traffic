﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Admin : System.Web.UI.Page
    {    
    
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                refresh();
            }
        }
        public void refresh()
        {
            DBC obj = new DBC();
            obj.ExcecuteQuery("Select * from Table_1");
            rptData.DataSource = obj.DT;
            rptData.DataBind();
        }
        protected void btnAccept_Click(object sender, EventArgs e)
        {
            DBC obj = new DBC();
            foreach (RepeaterItem i in rptData.Items)
            {
                CheckBox cb = (CheckBox)i.FindControl("chkCheck");
                if (cb.Checked)
                {
                    HiddenField hidden = (HiddenField)i.FindControl("hidden");
                    obj.ExcecuteQuery("Select * from Table_1 where no='"+hidden.Value+"' ");

                    obj.ExcecuteNonQuery("INSERT INTO Table_2 values('"+obj.DT.Rows[0][0].ToString()+ "','" + obj.DT.Rows[0][1].ToString() + "','" + obj.DT.Rows[0][2].ToString() + "','" + obj.DT.Rows[0][3].ToString() + "','" + obj.DT.Rows[0][4].ToString() + "','" + obj.DT.Rows[0][5].ToString() + "','" + obj.DT.Rows[0][6].ToString() + "','" + obj.DT.Rows[0][7].ToString() + "','" + obj.DT.Rows[0][8].ToString() + "','" + obj.DT.Rows[0][9].ToString() + "','" + obj.DT.Rows[0][10].ToString() + "')") ;
                    obj.ExcecuteNonQuery("DELETE FROM Table_1 WHERE no='" + hidden.Value + "' ");
                }
            }

            refresh();
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            DBC obj = new DBC();
            foreach (RepeaterItem i in rptData.Items)
            {
                CheckBox cb = (CheckBox)i.FindControl("chkCheck");
                if (cb.Checked)
                {
                    HiddenField hidden = (HiddenField)i.FindControl("hidden");
                    obj.ExcecuteNonQuery("DELETE FROM Table_1 WHERE no='" + hidden.Value + "'");
                }
            }

            refresh();
        }

       
    }
}