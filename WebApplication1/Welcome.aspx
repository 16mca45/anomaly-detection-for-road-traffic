﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="WebApplication1.Welcome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Road Anomaly | Welcome</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />

    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
</head>
<body>
     <div id="wrapper">
        <div id="page-wrapper" class="gray-bg" style="margin: 0 0 0 0px;">
            <div class="row border-bottom">
            </div> 
            <form id="form" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Welcome</h2>

                </div>
                <div class="col-sm-4">
                    <h2 id="lblErrror" runat="server" style="color:red"></h2>
                </div>

                

               
            </div>
           
            <div class="wrapper wrapper-content">
                <div class="middle-box text-center animated fadeInRightBig">
                    <h3 class="font-bold">Upload the dataset</h3>
                    <div class="error-desc">
                        Please select the file to upload from your PC. The data mining is done to the uploaded dataset.
                        <br /><br />
                        <asp:FileUpload ID="upload" runat="server" CssClass="btn btn-default" /><br /><br />
                        <asp:Button ID="btnSubmit" runat="server" Text="Upload" CssClass="btn btn-primary" OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
            </form>

        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
</body>
</html>
