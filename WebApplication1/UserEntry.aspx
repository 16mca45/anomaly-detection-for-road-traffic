﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserEntry.aspx.cs" Inherits="WebApplication1.UserEntry" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="icon" type="image/png" sizes="40x40" href="images/logo/3.png">
    <title></title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/default.css" rel="stylesheet" />
    <link href="css/plugins/footable/footable.core.css" rel="stylesheet">
    <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet" />

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="js/plugins/flot/jquery.flot.time.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <script src="js/plugins/footable/footable.all.min.js"></script>



</head>

<body>
    <form id="form1" runat="server">


        <div class="wrapper wrapper-content animated fadeInRight ecommerce gray-bg">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">





                        <div class="ibox-content">
                            <asp:ScriptManager ID="ScriptManager2" runat="server">
                            </asp:ScriptManager>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <h2>User Accident Entry</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">No of vehicles</label>
                                                <asp:TextBox ID="txtNofVehicles" runat="server" CssClass="form-control" required=""></asp:TextBox>
                                                
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Accident date</label>
                                                <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" required="" TextMode="DateTime"></asp:TextBox>
                                               
                                            </div>
                                        </div>

                                       

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Road</label>
                                                <asp:TextBox ID="txtRoad" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Road surface</label>
                                                <asp:DropDownList ID="ddlRdSrface" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Lighting Condition</label>
                                                <asp:DropDownList ID="ddlLightCondition" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Weather Conditon</label>
                                                <asp:DropDownList ID="ddlWeatherCondtn" runat="server" CssClass="form-control width" required=""></asp:DropDownList>
                                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Area</label>

                                               
                                                <asp:TextBox ID="txtArea" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Gender</label>
                                                <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control width" required=""></asp:DropDownList>
                                                <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Age</label>
                                                <asp:TextBox ID="txtAge" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Vehicle type</label>
                                                <asp:DropDownList ID="ddlVehType" runat="server" CssClass="form-control width" required=""></asp:DropDownList>
                                                <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>


                                    </div>
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary" OnClick="btnSubmit_Click" />

                                    <div class="clearfix"></div>
                                    </div><br />
                                    <br />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </form>
</body>



</html>
